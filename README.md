# The Movie Application
![Design homepage of the Movie Application](./images/homepage.png)

## Description
This is a Movie Application (one of my best project in Javascipt). With this application you can see movies who are playing, trending, upcoming and you can click on the picture to have more detail on the movie / actor. You can also search and fave a movie in your watchlist. 

## Contains
Call the api with fetch

Change mode theme in Dark or Light

Local storage

Gsap animation at the beginning

Double modal


## Technologies Used
Html

Scss

Javascript

theMovieDB API

Gsap


## Installation
git clone https://gitlab.com/Yutsu/movieapi


## Start Server
Live server from Vscode Extension and click "Go live"

## Ressources

All comes from undraw.io