export const animationGsap = () => {
    TweenMax.to(".overlay", 2, {
        delay: 2,
        top: "-130%",
        ease: Expo.easeInOut
    }),
    TweenMax.to(".overlay h1", 2, {
        delay: 1.5,
        opacity: 0,
        y: -60,
        ease: Expo.easeInOut
    }),
    TweenMax.to(".overlay span",2 , {
        delay: 1.8,
        opacity: 0,
        y: -60,
        ease: Expo.easeInOut
    }),
    TweenMax.from(".container", 2, {
        delay: 3,
        opacity: 0,
        ease: Expo.easeInOut
    })
}