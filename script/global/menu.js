import { getClassAll, getClass, getId } from './getElement.js';
import { clickModeTheme } from './localStorage.js';
import { getMoviesCategories } from './api.js';
import { displayMooflixResearch } from '../pages/mooflix.js';
import { displayMovieTrending } from '../pages/trending.js'
import { displayMovieUpcoming } from '../pages/upcoming.js';
import { displayMovieCategories, displayGenre, filterNote } from '../pages/categories.js';
import { displayFavMovie } from '../pages/watchlist.js';

const menuMode = getClass('.menu-mode');
const menuMobile = getClass('.menu');
let noteInput = getId('noteInput');

export const menuClick = () => {
    let menuLinks = getClassAll('.menu-link');
    let dataLinks = getClassAll('.data-menu');

    menuMode.addEventListener('click', () => {
        clickModeTheme();
    });

    menuLinks.forEach((menu, menuIndex) => {
        menu.addEventListener('click', () => {
            if(menu.classList.contains('active')){
                return;
            }
            else {
                menu.classList.add('active');
            }

            let index = menu.getAttribute('data-menuMovie');
            Array.from(menuLinks).map( reMenuLink => {
                if(reMenuLink.getAttribute('data-menuMovie') != index){
                    reMenuLink.classList.remove('active');
                }
            });

            dataLinks.forEach((infoLink, indexDataLink) => {
                if(menuIndex == indexDataLink){
                    infoLink.classList.remove('hide');
                    infoLink.classList.add('active');
                    menuMobile.classList.remove('menu-active');
                    
                } else {
                    infoLink.classList.add('hide');
                    infoLink.classList.remove('active');
                }
            });
        });
        switchMenu(menuIndex);
    });
}

const switchMenu = async(menuIndex) => {
    switch (menuIndex) {
        case 0:
            displayMooflixResearch();
            break;
        case 2:
            displayMovieTrending();
            break;
        case 3:
            displayMovieUpcoming();
            break;
        case 4:
            displayGenre();
            noteInput.addEventListener('input', filterNote);
            const movies = await getMoviesCategories();
            displayMovieCategories(movies);
            break;
        case 5:
            displayFavMovie();
            break;
        default:
            break;
    };
}
