//GLOBAL
const API_KEY = "04c35731a5ee918f014970082a0088b1";
const URL = "https://api.themoviedb.org/3";
const IMG_PATH = "https://image.tmdb.org/t/p/original";
const API_KEY_PATH = `api_key=${API_KEY}&language=en-US`;
const API_GENRES = ` ${URL}/genre/movie/list?${API_KEY_PATH}`;
const API_MOVIE_DETAIL = `${URL}/movie`;

//PAGES
const API_NOW_PLAYING = `${API_MOVIE_DETAIL}/now_playing?${API_KEY_PATH}&page=1`;
const API_TRENDING = `${URL}/trending/movie/week?api_key=${API_KEY}`;
const API_UPCOMING = `${API_MOVIE_DETAIL}/upcoming?${API_KEY_PATH}&page=1`;
const API_CATEGORIES = `${URL}/discover/movie?${API_KEY_PATH}&include_adult=false&include_video=false&page=1`;

//FETCH
const dataFetchResults = async(apiType) => {
    const result = await fetch(`${apiType}`, { origin: "cors" });
    const data = await result.json();
    return data.results;
}

const dataFetch = async(apiType) => {
    const result = await fetch(`${apiType}`, { origin: "cors" });
    const data = await result.json();
    return data;
}

//GLOBAL
export const getImages = (poster) => {
    return `${IMG_PATH}${poster}`;
}

export const getGenres = async() => {
    const result = await fetch(`${API_GENRES}`, { origin: "cors" });
    const data = await result.json();
    return data.genres;
}

//DETAIL MOVIE
export const getMovieDetail = async(id) => {
    return dataFetch(`${API_MOVIE_DETAIL}/${id}?${API_KEY_PATH}`)
}

export const getMovieCast = async(id) => {
    const result = await fetch(`${API_MOVIE_DETAIL}/${id}/credits?${API_KEY_PATH}`);
    const data = await result.json();
    return data;
}

export const getMovieTrailer = async(id) => {
    const result = await fetch(`${API_MOVIE_DETAIL}/${id}?api_key=${API_KEY}&append_to_response=videos`);
    const data = await result.json();
    return data;
}

export const getMovieReview = async(id) => {
    return dataFetchResults(`${API_MOVIE_DETAIL}/${id}/reviews?api_key=${API_KEY}&page=1`);
}

export const getMovieSimilar = async(id) => {
    return dataFetchResults(`${API_MOVIE_DETAIL}/${id}/similar?api_key=${API_KEY}&page=1`);
}

export const getActor = async(id) => {
    return dataFetch(`${URL}/person/${id}?${API_KEY_PATH}`);
}

export const getActorImg = async(id) => {
    const result = await fetch(`${URL}/person/${id}/images?api_key=${API_KEY}`, { origin: "cors" });
    const data = await result.json();
    return data.profiles;
}

export const getActorMovies = async(id) => {
    const result = await fetch(`${URL}/person/${id}/movie_credits?${API_KEY_PATH}`, { origin: "cors" });
    const data = await result.json();
    return data.cast;
}

//PAGES
export const getMoviesResearch = (movieResearch) => {
    return dataFetchResults(`${URL}/search/movie?${API_KEY_PATH}&query=${movieResearch}&page1&include_adult=false`);
}

export const getMoviesNowPlaying = () => {
    return dataFetchResults(API_NOW_PLAYING);
}

export const getMoviesTrending = () => {
    return dataFetchResults(API_TRENDING);
}

export const getMoviesUpcoming = () => {
    return dataFetchResults(API_UPCOMING);
}

export const getMoviesCategories = () => {
    return dataFetchResults(API_CATEGORIES);
}
