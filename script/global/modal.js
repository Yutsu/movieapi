import { getClass, getId } from '../global/getElement.js';
import * as api from '../global/api.js';
import { saveLocalMovies, removeLocalMovies } from '../global/localStorage.js';
import { displayFavMovie } from '../pages/watchlist.js';
const close = getClass('.close');
const modal_img = getClass('.modal-info-img');
const modal_info_movie = getClass('.modal-info-movie');

export const displayModal = async(id) => {
    let container_modal = getClass('.container-modal');
    container_modal.classList.remove('hide');
    let modal = getId('modal');
    modal.classList.remove('hide');

    let secondModal = getClass('.modal-container');
    let modal_cast = secondModal.querySelector('.modal-cast');
    modal_cast.classList.add('hide');

    const movie = await api.getMovieDetail(id);
    const { title, backdrop_path, vote_average: note, overview: synopsis } = movie;

    close.addEventListener('click', () => {
        container_modal.classList.add('hide');
        modal.classList.add('hide');
    });

    if(!backdrop_path && synopsis == ""){
        modal_img.innerHTML = "<h1>Empty description</h1>";
        modal_img.style.marginTop = "50px";
        modal_info_movie.innerHTML = `
            <div class="modal-info-between">
                <span><u class="big">Title:</u> ${title}</span>
            </div>
        `;
        return;
    }

    modal_img.style.marginTop = "0px";
    modal_img.innerHTML = `
        <img src=${api.getImages(backdrop_path)} class="modal-img" alt="${title}">
    `;

    let divStar = document.createElement('div');
    divStar.classList.add('movie-star');
    divStar.innerHTML = `
        <i class="far fa-star"></i>
    `;

    modal_info_movie.innerHTML = `
        <div class="modal-info-between">
            <span><u class="big">Title:</u> ${title}</span>
            <span><u class="big">Note:</u> ${note}</span>
        </div>
        <span><u class="big">Synopsis:</u> ${synopsis}</span>
        <div class="container-modal-btn">
            <span class="modal-btn">Read more</span>
        </div>
    `;
    modal_info_movie.appendChild(divStar);

    let star = getClass('.movie-star');
    star.addEventListener('click', () => {
        //if click not fav yet
        if(!star.classList.contains('fav')){
            divStar.classList.add('fav');
            saveLocalMovies(id);
            displayFavMovie();
        }
        //if click fav, delete fav
        else {
            star.classList.remove('fav');
            divStar.classList.remove('fav');
            removeLocalMovies(id);
            console.log(id);
            displayFavMovie();
        }
    });

    let idMovieFav = localStorage.getItem('idMovieFav');
    let idMovieFavUser = JSON.parse(idMovieFav);
    if(idMovieFavUser){
        idMovieFavUser.map(idMovie => {
            if(idMovie == id) {
                divStar.classList.add('fav');
            }
        });
    }

    const container_modal_btn = getClass('.container-modal-btn');
    container_modal_btn.addEventListener('click', () => {
        localStorage.setItem("idMovie", id);
        location.href = "movieDetail.html";
    });
}

export const displayModalCast = async(id) => {
    //ACTOR
    let container_modal = getClass('.container-modal');
    container_modal.classList.remove('hide');

    let modal = getId('modal');
    modal.classList.add('hide');

    let secondModal = getClass('.modal-container');
    let modal_cast = secondModal.querySelector('.modal-cast');
    modal_cast.classList.remove('hide');

    close.addEventListener('click', () => {
        container_modal.classList.add('hide');
    });


    let modal_info_cast = getClass('.modal-info-cast');
    let modal_info_cast_bio = getClass('.modal-info-biography');
    modal_info_cast.innerHTML = "";

    const infoActor = await api.getActor(id);
    const movies = await api.getActorMovies(id);
    const infoActorImage = await api.getActorImg(id);
    const date = new Date();


    const { biography, birthday, name, place_of_birth, popularity } = infoActor;
    const age = date.getFullYear() - birthday.substring(0, 4);

    const info_left = document.createElement('div');
    info_left.classList.add('modal-info-left');

    const info_right = document.createElement('div');
    info_right.classList.add('modal-info-right');

    let imageModal = infoActorImage.slice(-1).pop();
    info_left.innerHTML += `
         <img src=${api.getImages(imageModal.file_path)} class="modal-img-actor" alt="img">
    `;

    info_right.innerHTML += `
        <h1>${name}</h1>
        <span><u>Age:</u> ${age} years old</span>
        <span><u>Birthday:</u> ${birthday}</span>
        <span><u>Place of birth:</u> ${place_of_birth}</span>
        <span><u>Popularity:</u> ${popularity}</span>
    `;

    let biographyActor = "";
    (!biography) ?
        biographyActor = `none` :
        biographyActor = biography;

    modal_info_cast.appendChild(info_left)
    modal_info_cast.appendChild(info_right)
    modal_info_cast_bio.innerHTML = `<p><u>Biographie:</u> ${biographyActor} </p>`;


    //MOVIES
    const genres = await api.getGenres();
    const tabGenres = [];
    let tabDate = [];

    let modal_info_films = getClass('.modal-info-films');
    modal_info_films.innerHTML= "";

    for(let i = 0; i < 10; i++){
        if(!movies[i]){
            return;
        }
        const modal_films_card = document.createElement('div');
        modal_films_card.classList.add('modal-films-card');
        const { title, poster_path, genre_ids, character, release_date, id: idMovieActor } = movies[i];
        let { overview } = movies[i];

        tabDate.push(new Date(release_date));

        genre_ids.map(id => {
            genres.map(genre => {
                if(id == genre.id){
                    tabGenres.push(genre.name);
                }
            });
        });

        let displayGenre = "";
            (!tabGenres[1]) ?
                displayGenre = `<span>${tabGenres[0]}</span>` :
                displayGenre = `<span>${tabGenres[0]}/${tabGenres[1]}</span>`;

        if(overview.length > 280) {
            overview = `${overview.substring(0, 280)} [...]`;
        }

        let imgMovie = "";
        imgMovie = api.getImages(poster_path);
        if(api.getImages(poster_path) === "https://image.tmdb.org/t/p/originalnull"){
            imgMovie = "../../images/notfound.png";
        }
        modal_films_card.innerHTML = `
            <div class="modal-films-card-info">
                <img src=${imgMovie} class="modal-img-actor-film" alt="${title}">
                <div class="modal-films-card-text">
                    <span><u>Title:</u> ${title}</span>
                    <span><u>Genres:</u> ${displayGenre}</span>
                    <span><u>Character:</u> ${character}</span>
                    <span><u>Release date:</u> ${release_date}</span>
                    <span><u>Summary:</u> ${overview}</span>
                </div>
            </div>
        `;
        modal_info_films.appendChild(modal_films_card);

        if(api.getImages(poster_path) !== "https://image.tmdb.org/t/p/originalnull"){
            const clicked = modal_films_card.querySelector('.modal-img-actor-film');
            clicked.style.cursor = "pointer";
            clicked.addEventListener('click', () => {
                localStorage.setItem("idMovie", idMovieActor);
                location.href = "movieDetail.html";
            });
        }
    }
};