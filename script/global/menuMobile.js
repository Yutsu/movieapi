import { getClass } from './getElement.js';

export const menuMobileClick = () => {
    const hamburger = getClass('.hamburger');
    const menu = getClass('.menu');

    hamburger.addEventListener('click', () => {
        menu.classList.add('menu-active');
    });
    menu.addEventListener('click', () => {
        menu.classList.remove('menu-active');
    });
}