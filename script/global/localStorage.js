import { getClass } from '../global/getElement.js';

//MODE THEME
const body = getClass('body');

export const clickModeTheme = () => {
    const theme = window.localStorage.currentTheme;
    body.classList.add(theme);

    if(!body.classList.contains("dark")){
        body.classList.remove('light');
        body.classList.add('dark');
        localStorage.removeItem('currentTheme');
        localStorage.currentTheme = "dark";
    } else {
        body.classList.remove('dark');
        body.classList.add('light');
        localStorage.removeItem('currentTheme');
        localStorage.currentTheme = "light";
    }
}

export const checkThemeLoad = async(theme) => {
    body.classList.add(theme);
    !body.classList.contains("dark") ? body.classList.remove("dark") : body.classList.remove("light");
}


// WATCHLIST
const checkLocalMovie = () => {
    let movies;
    let storage = localStorage.getItem('idMovieFav');
    storage === null ? movies = [] : movies = JSON.parse(storage);
    return movies;
}

export const saveLocalMovies = (idMovieFav) => {
    let movies = checkLocalMovie();
    movies.push(idMovieFav);
    localStorage.setItem('idMovieFav', JSON.stringify(movies));
    //localStorage.clear();
}

export const removeLocalMovies = (idMovieFav) => {
    let movies = checkLocalMovie();
    movies.splice(movies.indexOf(idMovieFav), 1);
    localStorage.setItem('idMovieFav', JSON.stringify(movies))
}
