const getElement = (element) => {
    if(element){
        return element;
    } else {
        throw new Error("No element selected");
    }
}

export const getClassAll = (selectElement) => {
    const element = document.querySelectorAll(selectElement);
    return getElement(element);
};

export const getClass = (selectElement) => {
    const element = document.querySelector(selectElement);
    return getElement(element);
};

export const getId = (selectElement) => {
    const element = document.getElementById(selectElement);
    return getElement(element);
};
