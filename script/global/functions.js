import { getGenres } from './api.js';
import { displayModal } from '../global/modal.js';

export const noMovie = (movie_container, notFoundImg) => {
    const span = document.createElement('span');
    span.innerHTML= `No movie similar for this movie (yet)`;
    movie_container.style.justifyContent = "center";

    const img_not_found = document.createElement('div');
    img_not_found.classList.add('container-img-not-found');
    img_not_found.innerHTML = `
        <img src="../../images/Illustrations/${notFoundImg}" class="img-not-found" alt="img not found">
    `;

    movie_container.appendChild(span);
    movie_container.appendChild(img_not_found);
    return;
}

export const displayGenres = async(genre_ids) => {
    const genres = await getGenres();
    let tabGenres = [];
    genre_ids.map(id => {
        genres.map(genre => {
            if(id == genre.id){
                tabGenres.push(genre.name);
            }
        });
    });

    let displayGenre = "";
    (!tabGenres[1]) ?
        displayGenre = `${tabGenres[0]}` :
        displayGenre = `${tabGenres[0]}/${tabGenres[1]}`;
    return displayGenre;
}

export const displayTitle = (title, i) => {
    if(title.length > i){
        let titleMovie =`${title.substring(0, i)} ...`;
        return titleMovie;
    }
    return title;
}

export const clickModalImg = (clickImgModal, id) => {
    clickImgModal.addEventListener('click', () => {
        displayModal(id);
    });
}
