import { getClass, getId } from '../global/getElement.js';
import { getImages, getMoviesResearch, getGenres } from '../global/api.js';
import { displayModal } from '../global/modal.js';

const container = getClass('.film-research-cards');

export const displayMooflixResearch = () => {
    let form = getId('form');

    form.addEventListener('input', (e) => {
        e.preventDefault();
        let textMovie = form['search'].value;
        if(!textMovie) container.innerHTML = "";
    });

    form.addEventListener('submit', async(e) => {
        e.preventDefault();
        let textMovie = form['search'].value;
        if(!textMovie) container.innerHTML = "";

        const movies = await getMoviesResearch(textMovie);
        const genres = await getGenres();
        const tabGenres = [];

        if(movies.length == 0) {
            container.innerHTML = "";
            const span = document.createElement('span');
            span.innerHTML= `No movie founded`;

            const img_not_found = document.createElement('div');
            img_not_found.classList.add('container-img-not-found');
            img_not_found.innerHTML = `
                <img src="/images/Illustrations/no-found-search.png" class="img-not-found" alt="img not found">
            `;

            container.appendChild(span);
            container.appendChild(img_not_found);
            return;
        }

        container.innerHTML = '';

        movies.map(movie => {
            const card = document.createElement('div');
            card.classList.add('card');
            const { poster_path, genre_ids, id } = movie;
            let { title } = movie;

            genre_ids.map(id => {
                genres.map(genre => {
                    if(id == genre.id){
                        tabGenres.push(genre.name);
                    }
                });
            });

            if(title.length > 18){
                title = `${title.substring(0, 18)} ...`;
            }

            let displayGenre = "";
            (!tabGenres[1]) ?
                displayGenre = `<span>${tabGenres[0]}</span>` :
                displayGenre = `<span>${tabGenres[0]}/${tabGenres[1]}</span>`;

            if(getImages(poster_path) !== "https://image.tmdb.org/t/p/originalnull"){
                card.innerHTML = `
                    <img src=${getImages(poster_path)} class="research-card-img" alt="${title}">
                    <div class="research-card-info">
                        <span>${title}</span>
                        <span>${displayGenre}</span>
                    </div>
                `;
                container.appendChild(card);

                let clickImgModal = card.querySelector('.research-card-img');
                clickImgModal.addEventListener('click', () => {
                    displayModal(id);
                });
            }
        });
    });
}
