import { displayGenres, clickModalImg, displayTitle } from '../global/functions.js';
import { getClass } from '../global/getElement.js';
import { getMoviesUpcoming, getImages } from '../global/api.js';

const film_upcoming_cards = getClass('.film-upcoming-carrousel');

export const displayMovieUpcoming = async() => {
    const movies = await getMoviesUpcoming();

    for(let i = 0; i < 4; i++){
        const card = document.createElement('div');
        card.classList.add('film-upcoming-card');
        const { poster_path , id, genre_ids } = movies[i];
        let showGenres = await displayGenres(genre_ids);

        let { title } = movies[i];
        let titleMovie = displayTitle(title, 18);

        card.innerHTML = `
            <div class="film-upcoming-img">
                <img src=${getImages(poster_path)} class="upcoming-img" alt="${title}">
            </div>
            <div class="film-upcoming-info">
                <h2 class="span">${titleMovie}</h2>
                <span>${showGenres}</span>
            </div>
        `;
        film_upcoming_cards.appendChild(card);

        let clickImgModal = card.querySelector('.upcoming-img');
        clickModalImg(clickImgModal, id);
    }
}