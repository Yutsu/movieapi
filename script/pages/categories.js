import { displayTitle, clickModalImg } from '../global/functions.js';
import { getGenres, getMoviesCategories, getImages } from '../global/api.js';
import { getClass, getId } from '../global/getElement.js';

const containerCard = getClass('.film-categories-cards');

let noteInput = getId('noteInput');
let note = getClass('.note');

const filterByGenre = getClass('.filterByGenre');
const filterList = getClass('.filter-list');

const column1 = getClass('.column1');
const column2 = getClass('.column2');
const column3 = getClass('.column3');

const columnDisplay = (e) => {
    const genre = e.target.textContent;
    if(genre !== "All"){
        filterByGenre.innerHTML = `
            <p>${genre}<i class="fas fa-chevron-down arrow"></i></p>
        `;
    } else {
        filterByGenre.innerHTML = `
            <p>Filter by genre <i class="fas fa-chevron-down arrow"></i></p>
        `;
    }
    filterList.classList.add('hide');

    (genre !== "All") ? displayMoviesByGenre(genre) : displayAllMoviesByGenre();
}

filterByGenre.addEventListener('click', () => {
    filterList.classList.toggle('hide');
    column1.addEventListener('click', columnDisplay);
    column2.addEventListener('click', columnDisplay);
    column3.addEventListener('click', columnDisplay);
});

export const displayGenre = async() => {
    const genres = await getGenres();
    //Improve
    column1.innerHTML += `<li>All</li>`;
    for(let i = 0; i < 6; i++ ){
        column1.innerHTML += `
            <li>${genres[i].name}</li>
        `;
    }
    for(let j = 6; j < 13; j++ ){
        column2.innerHTML += `
            <li>${genres[j].name}</li>
        `;
    }
    for(let k = 13; k < 18; k++ ){
        column3.innerHTML += `
            <li>${genres[k].name}</li>
        `;
    }
    filterList.appendChild(column1);
    filterList.appendChild(column2);
    filterList.appendChild(column3);
}

const displayAllMoviesByGenre = async() => {
    const movies = await getMoviesCategories();
    displayMovieCategories(movies);
}

const displayMoviesByGenre = async(oneGenre) => {
    const movies = await getMoviesCategories();
    const genres = await getGenres();
    const tabMovies = [];

    movies.map(movie => {
        const { genre_ids } = movie;
        genre_ids.map(id => {
            genres.map(genre => {
                if(id == genre.id && oneGenre == genre.name){
                    tabMovies.push(movie);
                }
            });
        });
    });
    displayMovieCategories(tabMovies);
}

export const filterNote = async() => {
    const movies = await getMoviesCategories();
    const tabMovieNote = [];
    let noteValue = parseInt(noteInput.value);
    
    movies.map(movie => {
        const { vote_average: noteMovie } = movie;
        if(noteMovie <= noteValue){
            tabMovieNote.push(movie);
        }
    });
    note.textContent = `Note: ${noteValue}`;
    displayMovieCategories(tabMovieNote);
}

export const displayMovieCategories = async(movies) => {
    if(movies.length == 0){
        containerCard.innerHTML = ``;
        let span = document.createElement('span');
        span.innerHTML = "No film for this genre";

        const img_not_found = document.createElement('div');
        img_not_found.classList.add('container-img-not-found');
        img_not_found.innerHTML = `
            <img src="/images/Illustrations/not-found-genre.png" class="img-not-found" alt="img not found">
        `;

        containerCard.appendChild(span);
        containerCard.appendChild(img_not_found);
        return;
    }

    const genres = await getGenres();

    //Refresh
    containerCard.innerHTML = ``;

    movies.map(movie => {
        const tabGenres = [];
        const { genre_ids, poster_path, id } = movie;

        let { title } = movie;
        let showTitle = displayTitle(title, 18);

        genre_ids.map(id => {
            genres.map(genre => {
                if(id == genre.id){
                    tabGenres.push(genre.name);
                }
            });
        });

        let displayGenre = "";
        (!tabGenres[1]) ?
            displayGenre = `<span>${tabGenres[0]}</span>` :
            displayGenre = `<span>${tabGenres[0]}/${tabGenres[1]}</span>`;


        let card = document.createElement('div');
        card.classList.add('card');

        let img = document.createElement('img');
        img.src = getImages(poster_path);
        img.alt = title;
        img.classList.add('card-img');
        img.innerHTML = `<img src="${img.src}" alt="${img.alt}" class="card-img">`

        let cardInfo = document.createElement('div');
        cardInfo.classList.add('card-info');

        let h2 = document.createElement('h2');
        h2.innerHTML += showTitle;

        let span = document.createElement('span');
        span.innerHTML = `${displayGenre}`;

        cardInfo.appendChild(h2);
        cardInfo.appendChild(span);

        card.appendChild(img);
        card.appendChild(cardInfo);

        containerCard.appendChild(card);

        clickModalImg(img, id);
    });
}
