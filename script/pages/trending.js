
import { displayGenres, clickModalImg, displayTitle } from '../global/functions.js';
import { getMoviesTrending, getImages } from '../global/api.js';
import { getClass } from '../global/getElement.js';

const titleMovie = getClass('.film-trending-title');
const genreMovie = getClass('.film-trending-genre');

const filmCarrousel = getClass('.film-trending-carrousel');
const cards = getClass('.cards-trending');

export const displayMovieTrending = async() => {
    const movies = await getMoviesTrending();
    const { title, poster_path, genre_ids, id } = movies[0];

    const img = document.createElement('img');
    img.classList.add('img-carrousel-trending');
    img.src = getImages(poster_path);
    filmCarrousel.appendChild(img);

    let showGenres = await displayGenres(genre_ids);

    titleMovie.innerHTML = title;
    genreMovie.innerHTML = showGenres;

    for(let i = 1; i < 7; i++){
        const card = document.createElement('div');
        card.classList.add('card-trending');
        const { poster_path, genre_ids, id } = movies[i];

        let showGenres = await displayGenres(genre_ids);

        let { title } = movies[i];
        let titleMovie = displayTitle(title, 22);

        card.innerHTML = `
            <div class="container-img">
                <img src=${getImages(poster_path)} class="card-img" alt="${title}">
            </div>
            <div class="card-info">
                <h3>${titleMovie}</h3>
                <span>${showGenres}</span>
            </div>
        `;
        cards.appendChild(card);

        let clickImgModal = card.querySelector('.card-img');
        clickModalImg(clickImgModal, id);
    }

    let clickImgModal = document.querySelector('.img-carrousel-trending');
    clickModalImg(clickImgModal, id);
}