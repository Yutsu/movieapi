import { getMovieDetail, getImages } from '../global/api.js';
import { getClass } from '../global/getElement.js';
import { displayTitle, clickModalImg } from '../global/functions.js';

const watchlistMovie = getClass('.watchlist-cards');

export const displayFavMovie = async() => {
    let idMovieFav = localStorage.getItem('idMovieFav');
    // String to number
    let idMovieFavUser = JSON.parse(idMovieFav);

    //refresh
    watchlistMovie.innerHTML = "";
    if(idMovieFavUser){
    idMovieFavUser.map(async(item) => {
        const movie = await getMovieDetail(item);
        const tabGenres = [];

        const card = document.createElement('div');
        card.classList.add('card');
        const { poster_path, genres, id } = movie;
        let { title } = movie;

        genres.map(genre => {
            tabGenres.push(genre.name);
        });

        let displayGenre = "";
        (!tabGenres[1]) ?
            displayGenre = `<span>${tabGenres[0]}</span>` :
            displayGenre = `<span>${tabGenres[0]}/${tabGenres[1]}</span>`;

        let titleMovie = displayTitle(title, 18);

        card.innerHTML = `
            <img src=${getImages(poster_path)} class="card-img-fav" alt="${title}">
            <div class="card-info">
                <h2>${titleMovie}</h2>
                <span>${displayGenre}</span>
            </div>
        `;
        watchlistMovie.appendChild(card);

        let clickImgModal = card.querySelector('.card-img-fav');
        clickModalImg(clickImgModal, id);
    });
    }
}
