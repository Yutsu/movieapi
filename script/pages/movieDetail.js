import { clickModeTheme, checkThemeLoad } from '../global/localStorage.js';
import { saveLocalMovies, removeLocalMovies } from '../global/localStorage.js';
import { getClass } from '../global/getElement.js';
import { displayModalCast } from '../global/modal.js';
import { displayFavMovie } from '../pages/watchlist.js';

import * as api from '../global/api.js';
import * as func from '../global/functions.js';

const theme = window.localStorage.currentTheme;
const id = localStorage.getItem('idMovie');
const menuMode = getClass('.menu-mode');
const menuMobile = getClass('.menu');
const hamburger = getClass('.hamburger');
const returnPage = getClass('.container-return-btn');

const movie_img = getClass('.movie-img');
const movie_info_img = getClass('.movie-info-img');
const movie_info_left = getClass('.movie-info-left');
const movie_info_right = getClass('.movie-info-right');
const movie_info_synopsis = getClass('.movie-synopsis');

const movie_cast = getClass('.movie-cast-cards');
const movie_trailer = getClass('.movie-trailer-cards');
const movie_review = getClass('.movie-review-cards');
const movie_similar = getClass('.movie-similar-cards');

const point = (item) => {
    return item.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
}

const check = (item) => {
    if(item.length == 0) return item += "X";
    return item;
}

const menuTheme = () => {
    menuMode.addEventListener('click', () => {
        clickModeTheme();
    });
}

const movieDetail = async() => {
    const movie = await api.getMovieDetail(id);

    const { title, tagline, backdrop_path, release_date: date, vote_average: note, overview: summary } = movie;
    const { genres, poster_path, production_companies, runtime, budget, revenue } = movie;

    movie_img.innerHTML = `
        <img src=${api.getImages(backdrop_path)} class="movie-img" alt="${title}">
    `;

    const distribution = production_companies.map(compagnie => { return compagnie.name; });
    const genresMovie = genres.map(genre => { return genre.name; });

    let divStar = document.createElement('div');
    divStar.classList.add('movie-star-info');
    divStar.innerHTML = `
        <i class="far fa-star"></i>
    `;

    movie_info_img.innerHTML = `
        <img src=${api.getImages(poster_path)} class="movie-info-img" alt="${title}">
    `;
    movie_info_img.appendChild(divStar);

    let star = getClass('.movie-star-info');
    star.addEventListener('click', () => {
        //if click not fav yet
        if(!star.classList.contains('fav')){
            divStar.classList.add('fav');
            saveLocalMovies(id);
            displayFavMovie();
        }
        //if click fav, delete fav
        else {
            star.classList.remove('fav');
            divStar.classList.remove('fav');
            removeLocalMovies(id);
            displayFavMovie();
        }
    });

    let idMovieFav = localStorage.getItem('idMovieFav');
    let idMovieFavUser = JSON.parse(idMovieFav);
    if(idMovieFavUser){
        idMovieFavUser.map(idMovie => {
            if(idMovie == id) {
                divStar.classList.add('fav');
            }
        });
    }

    movie_info_left.innerHTML = `
        <p><u class="big">Title:</u> ${title}</p>
        <p><u class="big">Note:</u> ${check(note)}</p>
        <p><u class="big">Release date:</u> ${check(date)}</p>
        <p><u class="big">Tagline:</u> ${check(tagline)}</p>
    `;
    movie_info_right.innerHTML = `
        <p><u class="big">Distribution:</u> ${check(distribution.join(", "))}</p>
        <p><u class="big">Genre(s):</u> ${check(genresMovie.join(", "))}</p>
        <p><u class="big">Runtime:</u> ${check(runtime)}min</p>
        <p><u class="big">Budget:</u> ${point(budget)}$</p>
        <p><u class="big">Revenue:</u> ${point(revenue)}$</p>
    `;
    movie_info_synopsis.innerHTML = `
        <p><u class="big">Summary:</u> ${summary}</p>
    `;
}

const movieCast = async() => {
    const movie = await api.getMovieCast(id);
    const movieCast = movie.cast;

    for(let i = 0; i < 10; i++){
        const div = document.createElement('div');
        div.classList.add('card');
        let getImg = "";
        if(api.getImages(movieCast[i].profile_path) !== "https://image.tmdb.org/t/p/originalnull"){
            getImg = `<img src=${api.getImages(movieCast[i].profile_path)} class="movie-cast-img" alt="${movieCast[i].name}">`;
        } else {
            getImg = `<img src="../../images/nobody.jpg" class="movie-cast-nobody" alt="${movieCast[i].name}">`;
        }
        div.innerHTML = `
            ${getImg}
            <span>${movieCast[i].name}</span>
        `;
        movie_cast.appendChild(div);

        let clickImgModal = div.querySelector('.movie-cast-img');
        if(clickImgModal){
            clickImgModal.addEventListener('click', () => {
                displayModalCast(movieCast[i].id);
            });
        }
    }
}

const movieTrailer = async() => {
    const trailers = await api.getMovieTrailer(id);
    const videos = trailers.videos.results;
    const notFoundImg = "no-found-trailer.png";

    if(videos.length == 0) {
        func.noMovie(movie_trailer, notFoundImg);
        return;
    }

    videos.map(video => {
        const urlYb = `https://www.youtube.com/watch?v=${video.key}`;
        let url = urlYb.replace("watch?v=", "embed/");
        movie_trailer.innerHTML +=`
            <iframe width="450" height="345" class="video-youtube"
                src="${url}">
            </iframe>
        `;
    });
}

const movieReview = async() => {
    const reviews = await api.getMovieReview(id);
    const notFoundImg = "not-found-review.png";

    if(reviews.length == 0) {
        func.noMovie(movie_review, notFoundImg);
        return;
    }

    reviews.map(review => {
        const { author_details, updated_at, url } = review;
        let { content } = review;
        let note = "";
        const div = document.createElement('div');
        div.classList.add('card');

        if(content.length > 500){
            content = `${content.substring(0, 500)} [...]`;
        }
        (author_details.rating == null) ? note = "Didn't note" : note = author_details.rating;

        div.innerHTML += `
            <div class="card-info-left">
                <H2>${author_details.username}</H2>
                <span><u>Note:</u> ${note}</span>
                <span><u>Updated at:</u> ${updated_at.substring(0, 9)}</span>
                <a href="${url}" class="review-url" target="_blank"> See full review here > </a>
            </div>
            <div class="card-info-right">
                <span>${content}</span>
            </div>
        `;
        movie_review.appendChild(div);
    });
}

const movieSimilar = async() => {
    const movies = await api.getMovieSimilar(id);
    const notFoundImg = "not-found-similar.png";

    if(movies.length == 0) {
        func.noMovie(movie_similar, notFoundImg);
        return;
    }

    for(let i = 0; i < 8; i++){
        const div = document.createElement('div');
        div.classList.add('card');

        const { genre_ids, poster_path, id: idMovieSimilar } = movies[i];
        let { title } = movies[i];

        let titleMovie = func.displayTitle(title, 12);
        let showGenres = await func.displayGenres(genre_ids);

        if(api.getImages(poster_path) !== "https://image.tmdb.org/t/p/originalnull"){
            div.innerHTML += `
                <img src=${api.getImages(poster_path)} class="card-img" alt="${title}">
                <div class="card-info">
                    <h2>${titleMovie}</h2>
                    <span>${showGenres}</span>
                </div>
            `;
            movie_similar.appendChild(div);

            let clickImgModal = div.querySelector('.card-img');
            func.clickModalImg(clickImgModal, idMovieSimilar);
        }
    }
}

const returnPageApp = () => {
    returnPage.addEventListener('click', () => {
        location.href = "index.html";
        menuMobile.classList.add('menu-active');
    });
}

const menuMovieDetail = () => {
    hamburger.addEventListener('click', () => {
        menuMobile.classList.add('menu-active');
    });
    menuMobile.addEventListener('click', () => {
        menuMobile.classList.remove('menu-active');
    });
    returnPage.addEventListener('click', () => {
        menuMobile.classList.remove('menu-active');
    });
}

const app = async() => {
    await window.addEventListener('DOMContentLoaded', checkThemeLoad(theme));
    menuTheme();
    movieDetail();
    movieCast();
    movieTrailer();
    movieReview();
    movieSimilar();
    returnPageApp();
    menuMovieDetail();
}
app();
