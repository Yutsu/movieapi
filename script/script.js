import { animationGsap } from './global/animation.js';
import { menuClick } from './global/menu.js';
import { menuMobileClick } from './global/menuMobile.js';
import { checkThemeLoad } from './global/localStorage.js';
import { displayMoviePlaying } from './pages/playing.js';

const theme = window.localStorage.currentTheme;
const app = () => {
    checkThemeLoad(theme);
    displayMoviePlaying();
    animationGsap();
    menuClick();
    menuMobileClick();
}
app();
